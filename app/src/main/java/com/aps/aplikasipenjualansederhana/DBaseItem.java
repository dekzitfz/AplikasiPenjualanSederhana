package com.aps.aplikasipenjualansederhana;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

/**
 * Created by Administrator on 5/24/2016.
 */
public class DBaseItem{
    private SQLiteDatabase db;
    private Context context;
    private DbHelperBarang dbHelperBarang;

    DBaseItem(Context c)
    {
        context=c;
        dbHelperBarang=new DbHelperBarang(context,DbHelperBarang.NAMA_DB,null,DbHelperBarang.VERSI);
    }

    public void open()
    {
        try
        {
            db=dbHelperBarang.getWritableDatabase();
        }catch (SQLiteException er)
        {
            db=dbHelperBarang.getReadableDatabase();
        }
    }

    public void close()
    {
        db.close();
    }

    public void insertBarang(String kode, String nama, String harga, String kategori, String keterangan)
    {
        try {
            ContentValues dataBarang = new ContentValues();
            dataBarang.put(DbHelperBarang.KODE_BARANG, kode);
            dataBarang.put(DbHelperBarang.NAMA_BARANG, nama);
            dataBarang.put(DbHelperBarang.HARGA_BARANG, harga);
            dataBarang.put(DbHelperBarang.KATEGORI_BARANG, kategori);
            dataBarang.put(DbHelperBarang.KETERANGAN_BARANG, keterangan);

            db.insert(DbHelperBarang.NAMA_TABEL, null, dataBarang);
        } catch (SQLiteException er)
        {
            Log.d("Insert", er.getMessage());
        }
    }

    public Cursor getAllBarang()
    {
        return db.query(DbHelperBarang.NAMA_TABEL, new String[]{DbHelperBarang.ID_BARANG, DbHelperBarang.KODE_BARANG, DbHelperBarang.NAMA_BARANG,
                DbHelperBarang.HARGA_BARANG, DbHelperBarang.KATEGORI_BARANG, DbHelperBarang.KETERANGAN_BARANG}, null, null, null, null, null, null);
    }

    public boolean hapusBarang(int id)
    {
        return db.delete(DbHelperBarang.NAMA_TABEL,DbHelperBarang.ID_BARANG+"="+id,null)>0;
    }

    public boolean updateBarang(int id, String kode, String nama, String harga, String kategori, String keterangan)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbHelperBarang.KODE_BARANG, kode);
        contentValues.put(DbHelperBarang.NAMA_BARANG, nama);
        contentValues.put(DbHelperBarang.HARGA_BARANG, harga);
        contentValues.put(DbHelperBarang.KATEGORI_BARANG, kategori);
        contentValues.put(DbHelperBarang.KETERANGAN_BARANG, keterangan);

        return db.update(DbHelperBarang.NAMA_TABEL,contentValues,DbHelperBarang.ID_BARANG+"="+id,null)>0;
    }

    public Cursor getSelectBarang()
    {
        return db.query(DbHelperBarang.NAMA_TABEL, new String[]{DbHelperBarang.ID_BARANG, DbHelperBarang.KODE_BARANG, DbHelperBarang.NAMA_BARANG,
                DbHelperBarang.HARGA_BARANG, DbHelperBarang.KATEGORI_BARANG, DbHelperBarang.KETERANGAN_BARANG}, null, null, null, null, null, null);
    }
}
