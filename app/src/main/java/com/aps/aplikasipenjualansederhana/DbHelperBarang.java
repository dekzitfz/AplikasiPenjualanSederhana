package com.aps.aplikasipenjualansederhana;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Administrator on 5/24/2016.
 */
public class DbHelperBarang extends SQLiteOpenHelper{
    public final static  String NAMA_DB="aps";
    public final static int VERSI=1;
    public final static  String NAMA_TABEL="barang";
    public final static  String ID_BARANG="id";
    public final static  String KODE_BARANG="kode";
    public final static  String NAMA_BARANG="nama";
    public final static  String HARGA_BARANG="harga";
    public final static  String KATEGORI_BARANG="kategori";
    public final static  String KETERANGAN_BARANG="keterangan";

    private final String BUAT_TABEL="CREATE TABLE "+NAMA_TABEL+" ("+ID_BARANG+" integer Primary key autoincrement, "+
            KODE_BARANG+" text not null, "+NAMA_BARANG+" text not null, "+
            HARGA_BARANG+" text not null, "+KATEGORI_BARANG+" text not null, "+
            KETERANGAN_BARANG+" text not null);";

    public DbHelperBarang(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try
        {
            db.execSQL(BUAT_TABEL);
        }catch (SQLiteException er)
        {
            Log.d("SQL ", er.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " +NAMA_TABEL);
        onCreate(db);
    }
}