package com.aps.aplikasipenjualansederhana;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class RiwayatTransaksi extends AppCompatActivity {

    DBaseRiwayat dBaseRiwayat;
    Transaksi transaksi;
    ListView listViewing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat_transaksi);

        dBaseRiwayat = new DBaseRiwayat(this);

        listViewing = (ListView)findViewById(R.id.listTampilkan);
        ArrayList<Transaksi> transaksiArrayList = new ArrayList<Transaksi>();

        tampilTransaksi();
    }

    public void tampilTransaksi() {
        dBaseRiwayat.open();
        Cursor cursor = dBaseRiwayat.getAllTransaksi();
        ArrayList<Transaksi> transaksiArrayList = new ArrayList<Transaksi>();

        if (cursor.moveToFirst()) {
            do {
                transaksi = new Transaksi();
                transaksi.SetIdTransaksi(cursor.getInt(0));
                transaksi.setNamaBarang(cursor.getString(1));
                transaksi.setHargaBarang(cursor.getString(2));
                transaksi.setQty(cursor.getString(3));
                transaksi.setTotal(cursor.getString(4));

                transaksiArrayList.add(transaksi);
            } while (cursor.moveToNext());
        }

        TransaksiArrayAdapter adapter = new TransaksiArrayAdapter(this,transaksiArrayList);
        listViewing.setAdapter(adapter);
    }
}
