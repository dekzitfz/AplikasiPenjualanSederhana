package com.aps.aplikasipenjualansederhana;

/**
 * Created by Administrator on 5/24/2016.
 */
public class Transaksi {
    private int id;
    private String namanya, harganya, qty, total;

    /// -------------------------- kapsulasi -------------------------- ///
    // set
    public void SetIdTransaksi(int id){ this.id=id; }
    public void setNamaBarang(String namanya){ this.namanya=namanya; }
    public void setHargaBarang(String harganya){
        this.harganya=harganya;
    }
    public void setQty(String qty){
        this.qty=qty;
    }
    public void setTotal(String total){ this.total=total; }
    // get
    public int getIdTransaksi(){
        return id;
    }
    public String getNamaBarang(){
        return namanya;
    }
    public String getHargaBarang(){ return harganya; }
    public String getQty(){ return qty; }
    public String getTotal(){
        return total;
    }
    /// --------------------------------------------------------------- ///
}
