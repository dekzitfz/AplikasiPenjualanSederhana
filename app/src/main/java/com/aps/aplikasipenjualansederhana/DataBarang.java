package com.aps.aplikasipenjualansederhana;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class DataBarang extends AppCompatActivity {

    DBaseItem dBaseItem;
    Barang barang;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_barang);

        dBaseItem = new DBaseItem(this);

        listView = (ListView)findViewById(R.id.listTampil);
        ArrayList<Barang> barangArrayList = new ArrayList<Barang>();
        Button btnTambah = (Button)findViewById(R.id.btnTambah);

        tampilBarang();

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DataBarang.this, IsikanBarang.class);
                startActivity(i);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView txtId = (TextView) view.findViewById(R.id.txtId);
                TextView txtKode = (TextView) view.findViewById(R.id.txtKode);
                TextView txtNama = (TextView) view.findViewById(R.id.txtNama);
                TextView txtHarga = (TextView) view.findViewById(R.id.txtHarga);
                TextView txtKategori = (TextView) view.findViewById(R.id.txtKategori);
                TextView txtKeterangan = (TextView) view.findViewById(R.id.txtKeterangan);

                Intent i = new Intent(DataBarang.this, ManipulasiBarang.class);
                i.putExtra("id", txtId.getText());
                i.putExtra("kode", txtKode.getText());
                i.putExtra("nama", txtNama.getText());
                i.putExtra("harga", txtHarga.getText());
                i.putExtra("kategori", txtKategori.getText());
                i.putExtra("keterangan", txtKeterangan.getText());

                startActivity(i);
            }
        });
    }

    public void OnReSume() {
        super.onResume();
        tampilBarang();
    }

    public void tampilBarang()
    {
        dBaseItem.open();
        Cursor cursor = dBaseItem.getAllBarang();
        ArrayList<Barang> barangArrayList = new ArrayList<Barang>();

        if(cursor.moveToFirst())
        {
            do{
                barang = new Barang();
                barang.SetId(cursor.getInt(0));
                barang.setKode(cursor.getString(1));
                barang.setNama(cursor.getString(2));
                barang.setHarga(cursor.getString(3));
                barang.setKategori(cursor.getString(4));
                barang.setKeterangan(cursor.getString(5));

                barangArrayList.add(barang);
            }while (cursor.moveToNext());
        }

        BarangArrayAdapter adapter = new BarangArrayAdapter(this,barangArrayList);
        listView.setAdapter(adapter);
    }
}
