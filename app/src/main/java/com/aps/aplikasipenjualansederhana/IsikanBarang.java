package com.aps.aplikasipenjualansederhana;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class IsikanBarang extends AppCompatActivity {

    DBaseItem dBaseItem;
    EditText edtInKode, edtInNama, edtInHarga, edtInKeterangan;
    RadioButton radioInMakanan, radioInMinuman;
    Button btnInSimpanData;
    String Inkategori="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isikan_barang);

        dBaseItem = new DBaseItem(this);

        edtInKode=(EditText)findViewById(R.id.edtInKode);
        edtInNama=(EditText)findViewById(R.id.edtInNama);
        edtInHarga=(EditText)findViewById(R.id.edtInHarga);
        edtInKeterangan=(EditText)findViewById(R.id.edtInKeterangan);
        radioInMakanan=(RadioButton)findViewById(R.id.radioInMakanan);
        radioInMinuman=(RadioButton)findViewById(R.id.radioInMinuman);
        btnInSimpanData=(Button)findViewById(R.id.btnInSimpan);

        btnInSimpanData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(radioInMakanan.isChecked())Inkategori=radioInMakanan.getText().toString();
                if(radioInMinuman.isChecked())Inkategori=radioInMinuman.getText().toString();

                dBaseItem.open();
                dBaseItem.insertBarang(edtInKode.getText().toString(),edtInNama.getText().toString(),
                        edtInHarga.getText().toString(),Inkategori,edtInKeterangan.getText().toString());
                dBaseItem.close();

                Intent intent = new Intent(IsikanBarang.this, MainActivity.class);
                CharSequence notif = "Tambah Data berhasil! silahkan buka kembali data barang";
                Toast.makeText(getApplicationContext(), notif, Toast.LENGTH_LONG).show();
                setResult(RESULT_OK,intent);
                finish();
                startActivity(intent);
            }
        });

    }
}
