package com.aps.aplikasipenjualansederhana;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Administrator on 5/24/2016.
 */
public class BarangArrayAdapter extends ArrayAdapter {

    Context context;
    Barang barang;
    ArrayList<Barang> barangArrayList = new ArrayList<Barang>();

    public BarangArrayAdapter(Context context, ArrayList<Barang> barangArrayList) {
        super(context, R.layout.list_item, barangArrayList);
        this.context = context;
        this.barangArrayList = barangArrayList;
    }

    public View getView(int posisi, View view, ViewGroup ortu)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewRow = inflater.inflate(R.layout.list_item,ortu,false);

        TextView txtId = (TextView)viewRow.findViewById(R.id.txtId);
        TextView txtKode = (TextView)viewRow.findViewById(R.id.txtKode);
        TextView txtNama = (TextView)viewRow.findViewById(R.id.txtNama);
        TextView txtHarga = (TextView)viewRow.findViewById(R.id.txtHarga);
        TextView txtKategori = (TextView)viewRow.findViewById(R.id.txtKategori);
        TextView txtKeterangan = (TextView)viewRow.findViewById(R.id.txtKeterangan);

        barang = barangArrayList.get(posisi);
        txtId.setText(String.valueOf(barang.getId()));
        txtKode.setText(barang.getKode());
        txtNama.setText(barang.getNama());
        txtHarga.setText(barang.getHarga());
        txtKategori.setText(barang.getKategori());
        txtKeterangan.setText(barang.getKeterangan());

        return viewRow;
    }
}
