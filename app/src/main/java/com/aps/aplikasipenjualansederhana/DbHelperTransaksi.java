package com.aps.aplikasipenjualansederhana;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Administrator on 5/24/2016.
 */
public class DbHelperTransaksi extends SQLiteOpenHelper{
    public final static  String NAMA_DB="aps";
    public final static int VERSI=1;
    public final static  String NAMA_TABEL="transaksi";
    public final static  String ID_TRANSAKSI="id";
    public final static  String NAMA_BARANGNYA="namanya";
    public final static  String HARGA_BARANGNYA="harganya";
    public final static  String JUMLAH_QTY="qty";
    public final static  String TOTAL_TRANS="total";

    private final String BUAT_TABEL="CREATE TABLE "+NAMA_TABEL+" ("+ID_TRANSAKSI+" integer Primary key autoincrement, "+
            NAMA_BARANGNYA+" text not null, "+HARGA_BARANGNYA+" text not null, "+
            JUMLAH_QTY+" text not null, "+TOTAL_TRANS+" text not null);";

    public DbHelperTransaksi(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try
        {
            db.execSQL(BUAT_TABEL);
        }catch (SQLiteException er)
        {
            Log.d("SQL ", er.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " +NAMA_TABEL);
        onCreate(db);
    }
}