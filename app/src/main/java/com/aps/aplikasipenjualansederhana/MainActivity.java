package com.aps.aplikasipenjualansederhana;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    DBaseRiwayat dBaseRiwayat;
    EditText edtNamaBarang, edtHargaBarang, edtQty, edtTotal;
    Button btnEnter, btnTotal;
    DBaseItem dBaseItem; SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnData = (Button)findViewById(R.id.btnData);
        Button btnRiyawat = (Button)findViewById(R.id.btnRiwayat);

        btnData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent databarang = new Intent(MainActivity.this, DataBarang.class);
                startActivity(databarang);
            }
        });

        btnRiyawat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent riwayattransaksi = new Intent(MainActivity.this, RiwayatTransaksi.class);
                startActivity(riwayattransaksi);
            }
        });

        dBaseRiwayat = new DBaseRiwayat(this);

        edtNamaBarang=(EditText)findViewById(R.id.edtNamaBarang);
        edtHargaBarang=(EditText)findViewById(R.id.edtHargaBarang);
        edtQty=(EditText)findViewById(R.id.edtQty);
        edtTotal=(EditText)findViewById(R.id.edtTotal);
        btnEnter=(Button)findViewById(R.id.btnEnter);
        btnTotal=(Button)findViewById(R.id.btnTotal);

        btnTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String harganya = edtHargaBarang.getText().toString();
                int hrg = Integer.parseInt(harganya);

                String qty = edtQty.getText().toString();
                int q = Integer.parseInt(qty);

                edtTotal.setText(String.valueOf(hrg * q));
            }
        });

        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = edtNamaBarang.getText().toString();
                code ="";
                //if (code.matches("test")){
                //    edtNamaBarang.setText("Barang Percobaan");
                //    edtHargaBarang.setText("5000");
                //}
            }
        });

        Button btnSave = (Button)findViewById(R.id.btnSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dBaseRiwayat.open();
                dBaseRiwayat.insertTransaksi(edtNamaBarang.getText().toString(),edtHargaBarang.getText().toString(),
                        edtQty.getText().toString(),edtTotal.getText().toString());
                dBaseRiwayat.close();

                edtNamaBarang.getText().clear();
                edtHargaBarang.getText().clear();
                edtQty.getText().clear();
                edtTotal.getText().clear();

                CharSequence saved = "Transaksi Berhasil Disimpan";
                Toast.makeText(getApplicationContext(), saved, Toast.LENGTH_LONG).show();
            }
        });
    }
}
