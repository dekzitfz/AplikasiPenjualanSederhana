package com.aps.aplikasipenjualansederhana;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

/**
 * Created by Administrator on 5/26/2016.
 */
public class DBaseRiwayat {
    private SQLiteDatabase db;
    private Context context;
    private DbHelperTransaksi dbHelperTransaksi;

    DBaseRiwayat(Context c)
    {
        context=c;
        dbHelperTransaksi=new DbHelperTransaksi(context,DbHelperTransaksi.NAMA_DB,null,DbHelperTransaksi.VERSI);
    }

    public void open()
    {
        try
        {
            db=dbHelperTransaksi.getWritableDatabase();
        }catch (SQLiteException er)
        {
            db=dbHelperTransaksi.getReadableDatabase();
        }
    }

    public void close() { db.close(); }

    public void insertTransaksi(String namanya, String harganya, String qty, String total)
    {
        try {
            ContentValues dataTransaksi = new ContentValues();
            dataTransaksi.put(DbHelperTransaksi.NAMA_BARANGNYA, namanya);
            dataTransaksi.put(DbHelperTransaksi.HARGA_BARANGNYA, harganya);
            dataTransaksi.put(DbHelperTransaksi.JUMLAH_QTY, qty);
            dataTransaksi.put(DbHelperTransaksi.TOTAL_TRANS, total);

            db.insert(DbHelperTransaksi.NAMA_TABEL, null, dataTransaksi);
        } catch (SQLiteException er)
        {
            Log.d("Insert", er.getMessage());
        }
    }

    public Cursor getAllTransaksi()
    {
        return db.query(DbHelperTransaksi.NAMA_TABEL, new String[]{DbHelperTransaksi.ID_TRANSAKSI,DbHelperTransaksi.NAMA_BARANGNYA, DbHelperTransaksi.HARGA_BARANGNYA,
                DbHelperTransaksi.JUMLAH_QTY, DbHelperTransaksi.TOTAL_TRANS}, null, null, null, null, null, null);
    }
}
