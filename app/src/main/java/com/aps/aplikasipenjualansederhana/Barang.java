package com.aps.aplikasipenjualansederhana;

/**
 * Created by Administrator on 5/24/2016.
 */
public class Barang {
    private int id;
    private String kode, nama, harga, kategori, keterangan;

    /// -------------------------- kapsulasi -------------------------- ///
    // set
    public void SetId(int id){
        this.id=id;
    }
    public void setKode(String kode){ this.kode=kode; }
    public void setNama(String nama){ this.nama=nama; }
    public void setHarga(String harga){
        this.harga=harga;
    }
    public void setKategori(String kategori){
        this.kategori=kategori;
    }
    public void setKeterangan(String keterangan){
        this.keterangan=keterangan;
    }
    // get
    public int getId(){
        return id;
    }
    public String getKode(){
        return kode;
    }
    public String getNama(){
        return nama;
    }
    public String getHarga(){ return harga; }
    public String getKategori(){
        return kategori;
    }
    public String getKeterangan(){ return keterangan; }
    /// --------------------------------------------------------------- ///
}
