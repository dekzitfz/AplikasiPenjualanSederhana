package com.aps.aplikasipenjualansederhana;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Administrator on 5/24/2016.
 */
public class TransaksiArrayAdapter extends ArrayAdapter {

    Context context;
    Transaksi transaksi;
    ArrayList<Transaksi> transaksiArrayList = new ArrayList<Transaksi>();

    public TransaksiArrayAdapter(Context context, ArrayList<Transaksi> transaksiArrayList) {
        super(context,R.layout.list_riwayat,transaksiArrayList);
        this.context=context;
        this.transaksiArrayList=transaksiArrayList;
    }

    public View getView(int posisi, View view, ViewGroup ortu)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewRow = inflater.inflate(R.layout.list_riwayat,ortu,false);

        TextView txtIdTransaksi = (TextView)viewRow.findViewById(R.id.txtIdTransaksi);
        TextView txtNamaBarang = (TextView)viewRow.findViewById(R.id.txtNamaBarang);
        TextView txtHargaBarang = (TextView)viewRow.findViewById(R.id.txtHargaBarang);
        TextView txtQty = (TextView)viewRow.findViewById(R.id.txtQty);
        TextView txtTotal = (TextView)viewRow.findViewById(R.id.txtTotal);

        transaksi = transaksiArrayList.get(posisi);
        txtIdTransaksi.setText("No. Transaksi \t: " + String.valueOf(transaksi.getIdTransaksi()));
        txtNamaBarang.setText("Barang Laku \t\t: " + transaksi.getNamaBarang());
        txtHargaBarang.setText("Harga Satuan \t: " + transaksi.getHargaBarang());
        txtQty.setText("Jumlah Beli \t\t: " + transaksi.getQty());
        txtTotal.setText("Total Bayar \t\t: " + transaksi.getTotal());

        return viewRow;
    }
}
