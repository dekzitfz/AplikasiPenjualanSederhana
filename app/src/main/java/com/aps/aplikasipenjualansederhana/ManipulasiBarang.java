package com.aps.aplikasipenjualansederhana;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import android.widget.Toast;

public class ManipulasiBarang extends AppCompatActivity {

    EditText edtEdKode, edtEdNama, edtEdHarga, edtEdKeterangan;
    RadioButton radioEdMakanan, radioEdMinuman;
    Button btnEdSimpan, btnEdHapus;
    int id;

    DBaseItem dBaseItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manipulasi_barang);

        dBaseItem = new DBaseItem(this);

        btnEdSimpan=(Button)findViewById(R.id.btnEdSimpan);
        btnEdHapus=(Button)findViewById(R.id.btnEdHapus);
        edtEdKode=(EditText)findViewById(R.id.edtEdKode);
        edtEdNama=(EditText)findViewById(R.id.edtEdNama);
        edtEdHarga=(EditText)findViewById(R.id.edtEdHarga);
        radioEdMakanan=(RadioButton)findViewById(R.id.radioEdMakanan);
        radioEdMinuman=(RadioButton)findViewById(R.id.radioEdMinuman);
        edtEdKeterangan=(EditText)findViewById(R.id.edtEdKeterangan);

        Bundle ekstra = getIntent().getExtras();
        id=Integer.parseInt(ekstra.getString("id"));
        final String kategori = ekstra.getString("kategori");
        edtEdKode.setText(ekstra.getString("kode"));
        edtEdNama.setText(ekstra.getString("nama"));
        edtEdHarga.setText(ekstra.getString("harga"));
        if(kategori.equals(radioEdMakanan.getText()))radioEdMakanan.setChecked(true);
        if(kategori.equals(radioEdMinuman.getText()))radioEdMinuman.setChecked(true);
        edtEdKeterangan.setText(ekstra.getString("keterangan"));

        final Intent balik = new Intent(ManipulasiBarang.this, MainActivity.class);

        btnEdHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dBaseItem.open();
                boolean cek = dBaseItem.hapusBarang(id);
                CharSequence notif1 = "Hapus Data berhasil! silahkan buka kembali data barang";
                Toast.makeText(getApplicationContext(), notif1, Toast.LENGTH_LONG).show();
                setResult(RESULT_OK, balik);
                finish();
                startActivity(balik);

                dBaseItem.close();
            }
        });

        btnEdSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String kt="";
                if(radioEdMakanan.isChecked())kt=radioEdMakanan.getText().toString();
                if(radioEdMinuman.isChecked())kt=radioEdMinuman.getText().toString();

                dBaseItem.open();
                boolean cek = dBaseItem.updateBarang(id,edtEdKode.getText().toString(),
                        edtEdNama.getText().toString(),edtEdHarga.getText().toString(),
                        kt,edtEdKeterangan.getText().toString());
                dBaseItem.close();

                CharSequence notif2 = "Update Data berhasil! silahkan buka kembali data barang";
                Toast.makeText(getApplicationContext(), notif2, Toast.LENGTH_LONG).show();
                setResult(RESULT_OK, balik);
                finish();
                startActivity(balik);
            }
        });
    }
}
